import sublime

from .utils import tools as testtlib
from .utils import node
from .utils import env

# 根据语法格式化字符串
def beautifyStr(buffer_str, syntax, cursor_offset, options, TEST = False):
    if TEST:
        print({
            "syntax":syntax,
            "cursor_offset":cursor_offset,
            "options":options,
            })

    config_str = sublime.encode_value({
        "syntax":syntax, # 当前文件语法
        "cursorOffset":cursor_offset, # 当前鼠标位置
    })

    options_str = sublime.encode_value(options)
    
    node_script_str = env.get_node_script_path()


    res_str = node.run_script(node_script_str, config_str, options_str, buffer_str)

    if res_str:
        try:
            res_dict = sublime.decode_value(res_str)
            return res_dict
        except Exception:
            print(Exception('执行命令出错啦:',res_str))
            return False
