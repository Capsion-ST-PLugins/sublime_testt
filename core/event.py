import sublime_plugin
import os
import re

print('----------------------当前载入 event.py---------------------')

'''
@Description:
'''
# 每次打开文件都会触发这个事件
class ExecEventListener(sublime_plugin.EventListener):
    def on_load(self, view):
        # 设置所有缩进为2
        fn,ext = os.path.splitext(view.file_name())
        if ext == '.py':
            view.settings().set("tab_size", 4)
        else:
            view.settings().set("tab_size", 2)

        # 设置所有缩进为空格
        view.settings().set("translate_tabs_to_spaces": True,)
        view.settings().set("trim_trailing_white_space_on_save": True,)