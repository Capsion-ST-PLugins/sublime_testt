import sublime

_PACKAGE_SETTINGS = "testt.sublime-settings"
_PACKAGE_NAME = ''
settings_cache = {}

# 添加配置文件更新监听事件
def add_listener(PACKAGE_NAME):
    settings = sublime.load_settings(_PACKAGE_SETTINGS)
    settings.add_on_change(PACKAGE_NAME, _on_change)
    _PACKAGE_NAME = PACKAGE_NAME

def clear_listener():
    sublime.load_settings(_PACKAGE_SETTINGS).clear_on_change(_PACKAGE_NAME)


def _on_change():
    settings = sublime.load_settings(_PACKAGE_SETTINGS)

def get_settings():
    return sublime.load_settings(_PACKAGE_SETTINGS)

def get_options(syntax):
    # 获取配置对象
    settings = sublime.load_settings(_PACKAGE_SETTINGS)

    key = syntax+'_options'

    # 获取全局配置
    options = {}
    default_options = settings.get('global_options')

    # 如果 格式是vue 读取pug和stylus的配置
    if syntax == 'vue':
        options = update_options(['pug_options','stylus_options','vue_options'])

    elif syntax =='html2pug':
        return settings.get(key,{})

    else:
        # 获取对应语法的配置
        options = settings.get(key,{})

    # 返回全局配置
    default_options.update(options)
    
    return default_options
        
def update_options(list_options):
    options = {}
    settings = sublime.load_settings(_PACKAGE_SETTINGS)
    for each_options in list_options:
        options.update(settings.get(each_options,{}))

    return options