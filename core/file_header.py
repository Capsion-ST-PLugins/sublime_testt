# -*- coding: utf-8 -*-
#
# @Author: CPS
# @email: 373704015@qq.com
# @Date:
# @Last Modified by: CPS
# @Last Modified time: 2021-04-12 03:42:45.822704
# @file_path "D:\CPS\IDE\JS_SublmieText\Data\Packages\CPS\core"
# @Filename "file_header.py"
# @Description: 功能描述
#

if ( __name__ == "__main__"):
    pass


import sublime
import sublime_plugin
import datetime
import os

from . import settings
from .utils import env

TMPL_PATH = ""

def set_tmpl_path(plugin_name):
    global TMPL_PATH
    TMPL_PATH = os.path.join(sublime.packages_path(),plugin_name,"headerTmpl")

def get_tmpl_path():
    return TMPL_PATH

def insertFileHeader(filename, size=0):
    _settings = settings.get_settings()
    _options = settings.get_options('file_header')

    info = _options['headerInfo']

    # 文件字符串少于2的话,当作是一个新文件,添加创建时间
    if size <= 1:
        info = on_add_file(filename, info)
    info = on_modi_file(filename, info)

    # 获取对应的tmpl关系
    # tmplfile = get_tmpl_file(filename, _options)
    # print('get_tmpl_file....',tmplfile)
    # if not tmplfile: return

def get_header_info(filename, size=0):
    _settings = settings.get_settings()
    _options = settings.get_options('file_header')
    info = _options['headerInfo']

    # 文件字符串少于2的话,当作是一个新文件,添加创建时间
    if size <= 1:
        info = on_add_file(filename, info)
    info = on_modi_file(filename, info)
    return info

def get_header_settings():
    return settings.get_settings().get_options('file_header')

def get_tmpl_file(filename):
    _settings = settings.get_options('file_header')
    _template = _settings['template']
    ext = os.path.splitext(filename)[1].replace('.','').lower()
    if ext in _template.keys():
        if isinstance(_template[ext],list):
            # 弹出选项
            print('当前是列表中： ',_template[ext])
            # res = sublime.active_window().showSelectPanel(_template[ext])
            res={}
            for each in _template[ext]:
                res[each] = os.path.join(TMPL_PATH,each)+'.tmpl'
            return  res

        else:
            res = os.path.join(TMPL_PATH,_template[ext])+'.tmpl'
            return res

    return False

def replace_tmpl_info(tmpl_file, info):
    if os.path.exists(tmpl_file):
        # 读取tmpl文件
        res = ''
        with open(tmpl_file,'r',encoding='utf8') as tmpl:
            for each in tmpl.readlines():
                for key,val in info.items():
                    each = each.replace('{{'+key+'}}',val)
                res+=each
        return res


def on_add_file(filename,info):
    info['create_time'] = get_now(info['create_time'])
    return info

def on_modi_file(filename, info):
    info['last_modified_time'] = get_now(info['last_modified_time'])
    info['file_path'] = os.path.dirname(filename)
    fn = os.path.basename(filename)

    if fn.startswith('index'):
      # 如果文件是 index.xxxx, 就把文件夹名作为文件名
      info['file_name'] = os.path.basename(info['file_path'])
    else:
      info['file_name'] = fn

    return info

def get_now(fmat = r'%Y-%m-%d %H:%M:%S'):
    return datetime.datetime.now().__format__(fmat)

