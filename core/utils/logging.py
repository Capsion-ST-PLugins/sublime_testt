import sublime

from . import env

def log(*args):
	# 标题
	if env:
		print(env.get_plugin_name())
		
	print('=============================================')

	# 内容输出
	for each in range(1,len(args)):
		print(args[each])

	# 结尾
	print('----------------------------------------------')

