from subprocess import Popen, PIPE

NODEJS=False

commands = {
    'node':['node','-v'],
    'npm':['npm','-v'],
}

def check_node():
    res = run_command(commands['node']).strip()
    return True if res else False

def run_script(script, config, options, buffer_str):
    return run_command(['node', script, config, options], strBuffer=buffer_str)

# options 一个对象 可以携带 strBuffer
def run_command(command, strBuffer=None, shell=True):
    try:
        p = Popen(
            command,
            stdout=PIPE,
            stdin=PIPE,
            stderr=PIPE,
            shell=shell)

        # 过来一遍 strBuffer
        strBuffer = strBuffer.encode('utf-8') if strBuffer else strBuffer

        # 执行 command
        stdout, stderr = p.communicate(input=strBuffer, timeout=10000)

        if stdout:
            return stdout.decode('utf-8')
        else:
            print('没有返回有效的数据结果: \n%s' % stderr.decode('utf-8'))
            return ""

    except Exception:
        raise Exception('执行命令出错啦:',command)

if (__name__ == "__main__"):
    commands = r'C:\3176x32_suing\Data\Packages\testt\Shell\python\Python375_32\Scripts\uncompyle6.exe'

    folder = r'D:\CPS\MyProject\github\ST3_DoxyDoxygen\doxy_libs'

    import os,sys

    import threading

    file_list = os.listdir(folder)

    c = [commands,'-o',folder+r'\CrashReportHelpers.py',folder+r'\CrashReportHelpers.pyc']

    for each in file_list:
        (filename, extname) = os.path.splitext(each)
        if extname !='.pyc': continue

        pyc_file = os.path.join(folder,each)
        py_file = os.path.join(folder,filename+'.py')

        run_command([
                commands,
                '-o',
                py_file,
                pyc_file
            ],shell=False)


