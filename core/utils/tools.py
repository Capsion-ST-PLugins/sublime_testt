import os

def args_to_lower(func):
    def tes(*args,**kwargs):
        nargs = []
        for each in args:
            nargs.append(each.lower().strip())

        func(*nargs)
    return func

@args_to_lower
def is_stylus(filename):
    #通过 file_name 判断 stylus
    if filename.endswith('.stylus') or filename.endswith('.styl'):
        return True

@args_to_lower
def is_vue(filename):
    #通过 file_name 判断 vue
    if filename.endswith('.vue'):
        return True

@args_to_lower
def is_html(filename):
    #通过 file_name 判断 html
    if filename.endswith('.html') or filename.endswith('.xml'):
        return True

@args_to_lower
def is_pug(filename):
    #通过 file_name 判断 pug
    if filename.endswith('.pug'):
        return True

@args_to_lower
def is_js(filename):
    for each in ['.cjs','.mjs','.js']:
        if filename.endswith(each):
            return True

@args_to_lower
def is_ts(filename):
    for each in ['.ts']:
        if filename.endswith(each):
            return True

@args_to_lower
def is_json(filename):
    for each in ['.json']:
        if filename.endswith(each):
            return True

@args_to_lower
def is_python(filename):
    for each in ['.py']:
        if filename.endswith(each):
            return True


@args_to_lower
def sublime_syntax_check(syntax):
    for each in ['typescript','javascript','json','css','html']:
        if(syntax.lower().rfind(each)>0):
            return each
    return False


"""
Description

: param filename:{string} paramDescription
: returns {string} returnsDescription
"""
def check_stynax(filename):    
    if is_stylus(filename):
        return 'stylus'
    if is_vue(filename):
        return 'vue'
    if is_html(filename):
        return 'html'
    if is_pug(filename):
        return 'pug'
    if is_js(filename):
        return 'javascript'
    if is_ts(filename):
        return 'typescript'
    if is_json(filename):
        return 'json'
    if is_python(filename):
        return 'python'

    res = sublime_syntax_check(filename)

    return res if res else False

def get_date_now(self,fmat):
    try:
        if not fmat:
            fmat = r'%Y-%m-%d %H:%M:%S'
        return datetime.datetime.now().__format__(fmat)
    except Exception as e:
        return datetime.datetime.now().__format__(r'%Y-%m-%d %H:%M:%S')



if (__name__ == "__main__"):
    print(check_stynax('Packages/Python/Python.sublime-syntax'))
