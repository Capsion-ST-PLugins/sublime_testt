import sublime
import os

PLUGIN_NAME = "Sublime Testt"
PLUGIN_PATH = ""
CHANNEL_FILE = ""

def get_plugin_name():
	return PLUGIN_NAME

def set_plugin_name(plugin_name):
	global PLUGIN_NAME
	PLUGIN_NAME = plugin_name

	global PLUGIN_PATH
	PLUGIN_PATH = os.path.join(sublime.packages_path(),plugin_name)

def get_node_script_path():
	return os.path.join(sublime.packages_path(), PLUGIN_NAME, 'nodejs', 'main.js')

def channel_file_path():
	return os.path.join(sublime.packages_path(),PLUGIN_NAME,'core','channel_v3.json')

def check_file_exists(tar):
	return os.path.exists(tar)

def package_path():
	return os.path.join(sublime.packages_path(),PLUGIN_NAME)
